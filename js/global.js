//SOUNDS
var audio = [],
	audioPieces = [];

//TIMEOUTS
var timeout = [],
	launch = [],
	globalName,
	typingText;

var matches = function(ch, exp){
	for(var i = 0; i < exp.length; i++)
		if(exp[i] === ch)
			return true;
	
	return false;
}
	
function AudioPiece(source, start, end)
{
	this.audio = new Audio("audio/" + source + ".mp3");
	this.start = start;
	this.end = end;
	
	var currPiece = this,
		currAudio = this.audio;
	
	currAudio.addEventListener("timeupdate", function(){
		var currTime = Math.round(currAudio.currentTime);
		
		if(currTime == currPiece.end)
			currAudio.pause();
	});
}

AudioPiece.prototype.play = function()
{
	this.audio.currentTime = this.start;
	this.audio.play();
}

AudioPiece.prototype.pause = function()
{
	this.audio.pause();
}

AudioPiece.prototype.addEventListener = function(e, callBack)
{
	var currPiece = this,
		currAudio = this.audio,
		currDuration = Math.round(currAudio.duration);
	
	if(e === "ended")
	{
		console.log("else end: " + currPiece.end + ", " + currDuration);
		currAudio.addEventListener("timeupdate", function(){
			var currTime = Math.round(currAudio.currentTime);
				
			if(currTime === currPiece.end)
			{
				currAudio.pause();
				currAudio.currentTime += 3;
				callBack();
			}
		});
	}
	if(e === "timeupdate")
	{
		currPiece.audio.addEventListener("timeupdate", callBack);
	}
}

AudioPiece.prototype.getStart = function()
{
	return this.start;
}
	
function TypingText(jqueryElement, interval, hasSound, endFunction)
{
	this.text = jqueryElement.html();
	this.jqueryElement = jqueryElement;
	this.interval = interval;
	this.currentSymbol = 0;
	this.endFunction = endFunction;
	this.hasSound = hasSound;
	this.audio = new Audio("audio/keyboard-sound.mp3");
	this.audio.addEventListener("ended", function(){
			this.play();
		});
	
	this.timeout;
	this.jqueryElement.html("");
}

TypingText.prototype.write = function()
{
	tObj = this;
	
	if (tObj.audio.paused && tObj.hasSound) {
		tObj.audio.play();
	}
	tObj.timeout = setTimeout(function(){
		if(tObj.text[tObj.currentSymbol] === "<")
		{
			var numToPass = tObj.currentSymbol + 1, 
				openTag = "<",
				text = "", 
				closeTag = "";
				
			while(tObj.text[numToPass] !== ">")
			{
				openTag += tObj.text[numToPass];
				numToPass ++;
			}
			
			openTag += ">";
			
			numToPass ++;
			
			if(openTag === "<br>")
				tObj.jqueryElement.append(openTag);
			else
			{
				while(tObj.text[numToPass] !== "<")
				{
					text += tObj.text[numToPass];
					numToPass ++;
				}
				
				while(tObj.text[numToPass] !== ">")
				{
					closeTag += tObj.text[numToPass];
					numToPass ++;
				}
				
				closeTag += ">";
				numToPass ++;

				tObj.jqueryElement.append(openTag + text + closeTag);
			}
			
			tObj.currentSymbol = numToPass;
		}
		else
		{
			tObj.jqueryElement.append(tObj.text[tObj.currentSymbol]);
			tObj.currentSymbol ++;
		}
		if (tObj.currentSymbol < tObj.text.length) 
		{
			tObj.write();
		}
		else
		{
			tObj.audio.pause();
			tObj.endFunction();
		}
	}, tObj.interval);
}

TypingText.prototype.stopWriting = function()
{
	clearTimeout(this.timeout);
	this.audio.pause();
	this.jqueryElement.html(this.text);
}

var loadImages = function(){
	jQuery.get('fileNames.txt', function(data) {
		var imagesSrc = data.split("\n"),
			images = [],
			loadPercentage = 0,
			imagesNum = imagesSrc.length - 1;
			unitToAdd = Math.round(100 / imagesNum),
			preLoadImage = $("#pre-load .pre-load-image");
		
		var imageLoadListener = function(){
			loadPercentage += unitToAdd;
			preLoadImage.css("width", loadPercentage + "%");
			console.log(loadPercentage);
			imagesNum --;
			if (loadPercentage >= 100) {
				//hideEverythingBut($("#frame-000"));
			}
		};
		
		for (var i = 0; i < imagesSrc.length - 1; i ++)
		{
			images[i] = new Image();
			images[i].src = "pics/" + imagesSrc[i];
			images[i].addEventListener("load", imageLoadListener);
		}
	});
}

function DragTask(jqueryElements, successCondition, successFunction, failFunction, finishCondition, finishFunction)
{
	this.draggables = jqueryElements;
	this.draggabillies = [];
	this.vegetable;
	this.basket;
	
	this.makeThemDraggable = function()
	{
		for(var i = 0; i < this.draggables.length; i++)
			this.draggabillies[i] = new Draggabilly(this.draggables[i]);
	}
	
	this.addEventListeners = function()
	{
		for(var i = 0; i < this.draggabillies.length; i++)
		{
			this.draggabillies[i].on("dragStart", this.onStart);
			this.draggabillies[i].on("dragEnd", this.onEnd);
		}
	}
	
	this.onEnd = function(instance, event, pointer)
	{
		var currVeg = this.vegetable;
		var currBasket = this.basket;
		currVeg.fadeOut(0);
		currBasket = $(document.elementFromPoint(pointer.pageX, pointer.pageY));
		currVeg.fadeIn(0);
		
		if (currBasket.attr("data-key") && successCondition(currVeg.attr("data-key"), currBasket.attr("data-key")))
				successFunction(currVeg, currBasket);
		else
			failFunction(currVeg, currBasket);
		
		currVeg.removeClass("box-shadow-white");
		currVeg.css("opacity", "");
		
		if (finishCondition())
		{
			finishFunction();
		}
	}
	
	this.onStart = function(instance, event, pointer)
	{
		this.vegetable = $(event.target);
		this.vegetable.css("z-index", "9999");
		this.vegetable.addClass("box-shadow-white");
		this.vegetable.css("opacity", "0.6");
	}
	
	this.makeThemDraggable();
	this.addEventListeners();
}

var blink = function(jqueryElements, interval, times)
{
	var intervalHalf = Math.round(interval/2);
	timeout[0] = setTimeout(function(){
		jqueryElements.css("outline", "3px solid red");
		timeout[1] = setTimeout(function(){
			jqueryElements.css("outline", "");
			if (times) 
				blink(jqueryElements, interval, --times)		
		}, intervalHalf);
	}, intervalHalf);
}

var setLRSData = function(){
	tincan = new TinCan (
    {
        recordStores: [
            {
                "endpoint": "http://54.154.57.220/data/xAPI/",
				"username": "e083498f4e256e68ab2c5ae2be4195d9a348eb20",
				"password": "c6ea3c32a9d77919d0eb3cdea3bc5d460f50d93b"
            }
        ]
    });
}

var sendLaunchedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/launched"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var sendCompletedStatement = function(sm)
{
	if(globalName)
	{
		tincan.sendStatement(
		{
			"actor": {
				"objectType": "Agent",
				"account": {
					"name": globalName,
					"homePage": "http://lcms.nis.kz/chemistry"
				}
			},
			"verb": {
				"id": "http://adlnet.gov/expapi/verbs/completed"
			},
			"context": {
				"platform": "web"
			},
			"object": {
				"id": "http://lcms.nis.kz/activity/821eb006-58bb-4154-b732-4b2a9a9330ad",
				"objectType": "Activity",
				"description": "frame-" + sm
			}
		});
	}
}

var setMenuStuff = function(){
	var enterButton = $(".enter-button"),
		annotation = $("#frame-000 .annotation-hidden"),
		annotationButton = $("#frame-000 .annotation-button"),
		password = $(".password"),
		error = $(".error"),
		name = $(".name");
		name.val("");
		password.val("");
		
		
		sayHello = function(){
		regBox.html("Здравствуйте, " + name.val() + "!");
		regBox.css("height", "3em");
		regBox.css("padding", "0.5em");
		regBox.css("text-align", "center");
		globalName = name.val();
		regBox.addClass("topcorner");
		timeout[0] = setTimeout(function(){
			regBox.html(name.val());
			regBox.css("width", name.val().length + "em");
		}, 3000);
	};
	
	if(globalName)
		sayHello();
	
	var annotationButtonListener = function(){
		annotation.toggleClass("annotation-shown");
		annotationButton.toggleClass("annotation-button-close");
	};
	annotationButton.off("click", annotationButtonListener);
	annotationButton.on("click", annotationButtonListener);
	
	var enterButtonListener = function(){
		if(password.val() === "12345")
			sayHello();
		else
			error.html("неверный пароль");
	};
	enterButton.off("click", enterButtonListener);
	enterButton.on("click", enterButtonListener);
}


var arrayHas = function(arr, str){
	for (var i = 0; i < arr.length; i++)
		if(arr[i].indexOf(str) >= 0)
		{
			arr.splice(i, 1);
			return true;
		}
		
	return false;
}

launch["frame-000"] = function()
{
	
}

launch["frame-101"]  = function()
{
		theFrame = $("#frame-101"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		crystal = $(prefix + ".s2-crystal-glow"),
		textContainer = $(prefix + ".text-container");
		
	audio[0] = new Audio("audio/s1-1.mp3");
		
	audio[0].addEventListener("ended", function(){
		theFrame.attr("data-done", "true");
		hideEverythingBut($("#frame-102"));
		sendCompletedStatement(1);
	});
	
	var doneSecond = 0;
		
	fadeNavsOut();
	fadeLauncherIn();
	textContainer.fadeOut(0);
		
	startButtonListener = function(){
		textContainer.fadeIn(1000);
		audio[0].play();
		timeout[2] = setTimeout(function(){
			textContainer.removeClass("flipped");
		}, 2000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(1);
	}, 2000);
}

launch["frame-102"]  = function()
{
		theFrame = $("#frame-102"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		ionicCrystal = $(prefix + ".s2-ionic-crystal"),
		overSaturatedSolution = $(prefix + ".s2-oversaturated-solution"),
		crystalization = $(prefix + ".s2-crystalization"),
		solubility = $(prefix + ".s2-solubility"),
		vocabulary = $(prefix + ".vocabulary");
	
	audio[0] = new Audio("audio/s2-1.mp3");
		
	vocabulary.fadeOut(0);
		
	var doneSecond = 0;
	
	audio[0].addEventListener("ended", function(){
		setTimeout(function(){
			crystalization.fadeOut(500);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(2);
		}, 3000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
		
	startButtonListener = function(){
		audio[0].play();
		timeout[0] = setTimeout(function(){
			solubility.fadeIn(500);
		}, 5000);
		timeout[1] = setTimeout(function(){
			solubility.fadeOut(0);
			overSaturatedSolution.fadeIn(500);
		}, 10000);
		timeout[2] = setTimeout(function(){
			overSaturatedSolution.fadeOut(0);
			ionicCrystal.fadeIn(500);
		}, 16000);
		timeout[3] = setTimeout(function(){
			ionicCrystal.fadeOut(0);
			crystalization.fadeIn(500);
		}, 22000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(2);
	}, 2000);
}

launch["frame-103"]  = function()
{
		theFrame = $("#frame-103"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		cuprumSulfade = $(prefix + ".s3-cuprum-sulfade"),
		nickelChloride = $(prefix + ".s3-nickel-chloride"),
		potassiumPermanganat = $(prefix + ".s3-potassium-permanganat"),
		instructions = $(prefix + ".instructions"),
		pics = $(prefix + ".pic");
	
	audio[0] = new Audio("audio/s3-1.mp3");
			
	audio[0].addEventListener("timeupdate", function(){
		var currAudio = this, 
			currTime = Math.round(currAudio.currentTime), 
			doneSecond = 0;
			
		if(currTime === 8 && currTime !== doneSecond)
		{
			instructions.fadeIn(500);
		}
	});
	audio[0].addEventListener("ended", function(){
		timeout[3] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(3);
		}, 3000);
	});
	
	fadeNavsOut();
	fadeLauncherIn();
	pics.fadeOut(0);
		
	startButtonListener = function(){
		audio[0].play();
		timeout[0] = setTimeout(function(){
			cuprumSulfade.fadeIn(500);
		}, 1000);
		timeout[1] = setTimeout(function(){
			nickelChloride.fadeIn(500);
		}, 2000);
		timeout[2] = setTimeout(function(){
			potassiumPermanganat.fadeIn(500);
		}, 3000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(3);
	}, 2000);
}

launch["frame-104"]  = function()
{
		theFrame = $("#frame-104"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		labels = $(prefix + ".label"),
		items = $(prefix + ".item"),
		glass = $(prefix + ".glass"),
		spoon = $(prefix + ".spoon"),
		glassMix = $(prefix + ".glass-mix"),
		glassPour = $(prefix + ".glass-pour"),
		petry = $(prefix + ".petry"),
		thermometer = $(prefix + ".thermometer"),
		saltCan = $(prefix + ".salt-can"),
		tasks = $(prefix + ".task"),
		task1 = $(prefix + ".task-1"),
		task2 = $(prefix + ".task-2"),
		task3 = $(prefix + ".task-3"),
		task4 = $(prefix + ".task-4"),
		spoonNum = 0;
	
	thermSprite = new Motio(thermometer[0], {
		"fps": "2",
		"frames": "4"
	});
	thermSprite.on("frame", function(){
		if (this.frame === this.frames - 1)
		{
			this.pause();
			timeout[0] = setTimeout(function(){
				audio[3].play();
			}, 3000);
			timeout[0] = setTimeout(function(){
				audio[3].pause();
				theFrame.attr("data-done", "true");
				fadeNavsInAuto();
				sendCompletedStatement(1);
			}, 29000);
		}
	});
	
	glassSprite = new Motio(glass[0], {
		"fps": "2",
		"frames": "5"
	});
	glassSprite.to(3, true);
	
	glassMixSprite = new Motio(glassMix[0], {
		"fps": "1",
		"frames": "6"
	});
	
	spoonSprite = new Motio(spoon[0], {
		"fps": "2",
		"frames": "3"
	})
	
	petrySprite = new Motio(petry[0], {
		"fps": "3",
		"frames": "5"
	});
	
	glassPourSprite = new Motio(glassPour[0], {
		"fps": "2",
		"frames": "3"
	});
	
	glassPourSprite.on("frame", function(){
		if (this.frame === this.frames - 1) {
			this.pause();
			audio[2].pause();
			glassPour.fadeOut(0);
			glass.fadeIn(0);
			glass.css("left", "40%");
			var glassPos = glassSprite.frame;
			glassPos --;
			glassSprite.to(glassPos, true);
			if (!glassPos)
			{
				glass.fadeOut(0);
				tasks.fadeOut(0);
				task3.fadeIn(0);
				thermometer.fadeIn(0);
				
				var thermSuccessIf = function(vegetable, basket)
				{
					return basket === "petry";
				}
				
				thermSuccessFunc = function(vegetable, basket)
				{
					vegetable.css({
						"top": "35%",
						"left": "65%"
					});
					thermSprite.play();
				}
				
				var dragTask3 = new DragTask(thermometer, thermSuccessIf, thermSuccessFunc, function(){}, function(){}, function(){});
			}
		}
	});
	
	audio[0] = new Audio("audio/salt-sound.mp3");
	audio[1] = new Audio("audio/water-stir.mp3");
	audio[2] = new Audio("audio/water-pour.mp3");
	audio[3] = new Audio("audio/s4-1.mp3");
	
	glassMixSprite.on("frame", function(){
		if (this.frame === 1) {
			spoonSprite.to(1, true);
		}
		if (this.frame === 2) {
			spoonSprite.to(0, true);
			spoon.css("left", "40%");
			spoon.css("top", "10%");
			audio[1].currentTime = 0;
			audio[1].play();
		}
		if (this.frame === this.frames - 1) {
			this.pause();
			glassMix.fadeOut(0);
			glassSprite.to(3, true);
			glass.fadeIn(0);
			audio[1].pause();
			audio[0].currentTime = 0;
		}
	});
		
	var successCondition = function(vegetable, basket)
	{
		return basket === "salt-can" || basket === "salt-can-2" || basket === "glass" || basket === "petry";
	}
	
	var successFunction = function(vegetable, basket)
	{
		if (basket.attr("data-key") === "salt-can")
		{
			audio[0].play();
			spoonSprite.to(2, true);
		}
		if (basket.attr("data-key") === "salt-can-2")
		{
			audio[0].play();
			spoonSprite.to(1, true);
		}
		if (basket.attr("data-key") === "glass" && spoonSprite.frame === 2)
		{
			vegetable.fadeOut(0);
			glass.fadeOut(0);
			glassMixSprite.to(1, true);
			glassMix.fadeIn(0);
			spoon.fadeIn(0);
			spoon.css("left", "15%");
			spoon.css("top", "-10%");
			
			timeout[0] = setTimeout(function(){
				glassMixSprite.play();
			}, 1000);
			
			spoonNum ++;
			if (spoonNum === 4) {
				tasks.fadeOut(0);
				task2.fadeIn(500);
				glass.attr("data-key", "glass-no");
				saltCan.attr("data-key", "salt-can-2");
				petry.fadeIn(0);
			}
		}
		if (basket.attr("data-key") === "petry" && spoonSprite.frame === 1)
		{
			petrySprite.to(1, true);
			saltCan.fadeOut(0);
			spoon.fadeOut(0);
			
			var glassSuccessIf = function(vegetable, basket)
			{
				return basket === "petry";
			}
			
			var glassSuccessFunc = function(vegetable, basket)
			{
				var petryFrame = petrySprite.frame;
				petryFrame ++;
				glass.fadeOut(0);
				glassPour.fadeIn(0);
				audio[2].play();
				glassPourSprite.play();
				petrySprite.to(petryFrame, true);
				
				if (petryFrame === petrySprite.frames - 1) {
					glass.fadeOut(0);
				}
			}
			
			var dragTask2 = new DragTask(glass, glassSuccessIf, glassSuccessFunc, function(){}, function(){});
		}
	}
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
	}
	
	var dragTask = new DragTask(spoon, successCondition, successFunction, failFunction, function(){}, function(){});
	
	fadeNavsOut();
	fadeLauncherIn();
	tasks.fadeOut(0);
	labels.fadeOut(0);
	glassMix.fadeOut(0);
	glassPour.fadeOut(0);
	thermometer.fadeOut(0);
	items.css({
		"background-color": "transparent",
		"border-color": "transparent"
	});
		
	startButtonListener = function(){
		items.css({
			"background-color": "",
			"border-color": ""
		});
		labels.fadeIn(0);
		timeout[0] = setTimeout(function(){
			items.css({
				"background-color": "transparent",
				"border-color": "transparent"
			});
			labels.fadeOut(0);
			items.fadeOut(0);
			glass.fadeIn(0);
			spoon.fadeIn(0);
			saltCan.fadeIn(0);
			task1.fadeIn(500);
		}, 5000);
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(4);
	}, 2000);
}

launch["frame-105"]  = function()
{
		theFrame = $("#frame-105"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		draggables = $(prefix + ".ball"),
		award = $(prefix + ".award"),
		snowfall = $(prefix + ".snowfall"),
		saltContainer = $(".salt-container");
	
	audio[0] = new Audio("audio/award-sound.mp3");
	audio[1] = new Audio("audio/s5-1.mp3");
	
	var snowfallSprite = new Motio(snowfall[0], {
		fps: 3,
		frames: 3
	});
	
	award.fadeOut(0);
	snowfall.fadeOut(0);
	
	audio[1].addEventListener("ended", function(){
		timeout[0] = setTimeout(function(){
			snowfall.fadeOut(0);
			award.fadeOut(0);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 3000);
	});
	
	var successCondition = function(vegetableKey, basketKey)
	{
		return basketKey.indexOf(vegetableKey) >= 0;
	}
	
	var successFunction = function(vegetable, basket)
	{
		basket.html(vegetable.html());
		vegetable.remove();
	}
	
	var failFunction = function(vegetable, basket)
	{
		vegetable.css("left", "");
		vegetable.css("top", "");
		vegetable.css("z-index", "");
		vegetable.removeClass("box-shadow-white");
		vegetable.css("opacity", "");
	}
	
	var finishCondition = function()
	{
		return !$(prefix + " .ball").length;
	}
	
	var finishFunction = function()
	{
		timeout[0] = setTimeout(function(){
			audio[0].play();
			award.fadeIn(1000);
			snowfall.fadeIn(1000);
			snowfallSprite.play();
			saltContainer.append("<div class='salt'></div>");
		}, 2000);
		timeout[1] = setTimeout(function(){
			audio[1].play();
		}, 6000);
	}
	
	var dragTask = new DragTask(draggables, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	fadeNavsOut();
	fadeLauncherIn();
		
	startButtonListener = function(){
		
	};
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(5);
	}, 2000);
}

launch["frame-106"]  = function()
{
		theFrame = $("#frame-106"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		exercises = $(prefix + ".exercise"), 
		answers = $(prefix + ".answer"),
		checkButton = $(prefix + ".check-button"),
		answerfields = $(prefix + ".answerfield"),
		result = $(prefix + ".result"),
		award = $(prefix + ".award"),
		saltContainer = $(".salt-container"),
		snowfall = $(prefix + ".snowfall"),
		correctNum = 0,
		fieldCorrectAnswers = ["Арал Аральское", "Каспий Каспийское"];
	
	audio[0] = new Audio("audio/s4-1.mp3");
	audio[1] = new Audio("audio/award-sound.mp3");
	audio[2] = new Audio("audio/s6-1.mp3");
	
	snowfallSprite = new Motio(snowfall[0], {
		"fps": "3",
		"frames": "3"
	});
		
	fadeNavsOut();
	fadeLauncherIn();
	result.fadeOut(0);
	award.fadeOut(0);
	snowfall.fadeOut(0);
	answerfields.val("");
	checkButton.fadeOut(0);
		
	startButtonListener = function(){
		theFrame.attr("data-done", "true");
		audio[2].play();
		timeout[0] = setTimeout(function(){
			audio[2].pause();
		}, 9000);
	};

	var answerListener = function(){
		var answer = $(this);
		answer.addClass("chosen");
		answer.siblings().removeClass("chosen");
		
		if($(prefix + ".chosen").length >= 3 && 
			$(answerfields[0]).val() != "" &&
			$(answerfields[1]).val() != "")
			checkButton.fadeIn(0);
	};
	answers.off("click", answerListener);
	answers.on("click", answerListener);
	
	var answerFieldsListener = function(){
		if($(prefix + ".chosen").length >= 3 && 
			$(answerfields[0]).val() != "" &&
			$(answerfields[1]).val() != "")
			checkButton.fadeIn(0);
	};
	answerfields.off("keyup", answerFieldsListener);
	answerfields.on("keyup", answerFieldsListener);
	
	var checkListener = function(){
		for(var i = 0; i < answerfields.length; i++)
		{
			currfield = $(answerfields[i]);
			if (arrayHas(fieldCorrectAnswers, currfield.val()))
			{
				correctNum ++;
				currfield.css("color", "green");
				currfield.css("font-weight", "bold");
			}
			else
				$(answerfields[i]).css("color", "red");
		}
		
		for(var i = 0; i < answers.length; i++)
		{
			if($(answers[i]).hasClass("chosen"))
			{
				if($(answers[i]).attr("data-correct"))
				{
					correctNum ++;
					$(answers[i]).css("color", "green");
					$(answers[i]).css("font-weight", "bold");
				}
				else 
					$(answers[i]).css("color", "red");
			}
		}
		
		result.append("<br>" + correctNum);
		result.fadeIn(0);
		if (correctNum >= 5)
		{
			result.addClass("result-correct");
			timeout[0] = setTimeout(function(){
				audio[1].play();
				result.fadeOut(0);
				award.fadeIn(0);
				snowfall.fadeIn(0);
				snowfallSprite.play();
				saltContainer.append("<div class='salt'></div>");
			}, 3000);
		}
		else
			result.addClass("result-false");
		
		fieldCorrectAnswers = ["Арал Аральское", "Каспий Каспийское"];
		
		timeout[1] = setTimeout(function(){
			var afield0 = $(answerfields[0]),
				afield1 = $(answerfields[1]);
				
			if(afield0.css("color") === "rgb(255, 0, 0)")
			{
				if(afield1.val().indexOf("Арал") >= 0)
				{
					afield0.val("Каспий");
				}
				else if(afield1.val().indexOf("Каспий") >= 0)
				{
					afield0.val("Арал");
				}
				else
				{
					afield0.val("Арал");
				}
				
				afield0.css("font-weight", "bold");
				afield0.css("color", "green");
			}
			if(afield1.css("color") === "rgb(255, 0, 0)")
			{
				if(afield0.val().indexOf("Арал") >= 0)
				{
					afield1.val("Каспий");
				}
				else if(afield0.val().indexOf("Каспий") >= 0)
				{
					afield1.val("Арал");
				}
				else
				{
					afield1.val("Каспий");
				}
				
				afield1.css("font-weight", "bold");
				afield1.css("color", "green");
			}
						
			for(var i = 0; i < answers.length; i++)
			{
				if($(answers[i]).hasClass("chosen"))
				{
					if($(answers[i]).attr("data-correct"))
					{
						$(answers[i]).css("color", "green");
						$(answers[i]).css("font-weight", "bold");
					}
					else 
						$(answers[i]).css("color", "red");
				}
				else
				{
					if($(answers[i]).attr("data-correct"))
					{
						$(answers[i]).css("color", "green");
						$(answers[i]).css("font-weight", "bold");
					}
				}
			}
		}, 3000);
		timeout[2] = setTimeout(function(){
			audio[2].currentTime = 11;
			audio[2].play();
		}, 8000);
		timeout[2] = setTimeout(function(){
			result.fadeOut(0);
			award.fadeOut(0);
			snowfallSprite.pause();
			snowfall.fadeOut(0);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 18000);
	};
	checkButton.off("click", checkListener);
	checkButton.on("click", checkListener);
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(6);
	}, 2000);
}

launch["frame-107"]  = function()
{
		theFrame = $("#frame-107"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		draggables = $(prefix + ".ball"),
		award = $(prefix + ".award"),
		snowfall = $(prefix + ".snowfall"),
		result = $(prefix + ".result"),
		correctNum = 0,
		checkButton = $(prefix + ".check-button"),
		answerfields = $(prefix + ".answerfield"),
		saltContainer = $(".salt-container");
	
	audio[0] = new Audio("audio/award-sound.mp3");
	audio[1] = new Audio("audio/s7-1.mp3");
	
	var fieldsNotEmpty = function(){
		for(var i = 0; i < answerfields.length; i++)
			if($(answerfields[i]).val() === "")
				return false;
		
		return true;
	};
	
	var snowfallSprite = new Motio(snowfall[0], {
		fps: 3,
		frames: 3
	});
	
	var successCondition = function(vegetableKey, basketKey){
		return vegetableKey === basketKey;
	};
	var successFunction = function(vegetable, basket){
		basket.html(vegetable.html());
		vegetable.remove();
		correctNum++;
	};
	var failFunction = function(vegetable, basket)
	{
		vegetable.fadeIn(0);
		vegetable.css("left", "");
		vegetable.css("top", "");
	}
	
	var finishCondition = function()
	{
		return !$(prefix + " .ball").length && fieldsNotEmpty();
	}
	
	var finishFunction = function()
	{
		checkButton.fadeIn(0);
	}
	
	var dragTask = new DragTask(draggables, successCondition, successFunction, failFunction, finishCondition, finishFunction);
	
	fadeNavsOut();
	fadeLauncherIn();
	award.fadeOut(0);
	snowfall.fadeOut(0);
	result.fadeOut(0);
	checkButton.fadeOut(0);
	
	answerfields.val("");
		
	startButtonListener = function(){
		audio[1].play();
	};
	
	var answerfieldsListener = function(){
		if(!$(prefix + " .ball").length && fieldsNotEmpty())
			checkButton.fadeIn(0);
		
		currField = $(this);
		var exp = ["0","1","2","3","4","5","6","7","8","9"];
		var currValue = currField.val();
		
		if(matches(currValue.charAt(currValue.length - 1), exp))
		{
			$(prefix + "." + currField.attr("data-key")).html(currValue);
		}
		else
		{
			var currValueArray = currValue.split("");
			currValueArray.pop();
			currValue = currValueArray.join("");
			currField.val(currValue);
		}
	};
	answerfields.off("keyup", answerfieldsListener);
	answerfields.on("keyup", answerfieldsListener);
	
	var checkListener = function(){
		for(var i = 0; i < answerfields.length; i++)
		{
			var min = parseInt($(answerfields[i]).attr("data-key-min"));
			var max = parseInt($(answerfields[i]).attr("data-key-max"));
			var answer = parseInt($(answerfields[i]).val());
			
			if ((answer >= min) && (answer <= max)) {
				correctNum ++;
				$(answerfields[i]).css("color", "green");
			}
			else
				$(answerfields[i]).css("color", "red");
		}
		
		result.append("<br>" + correctNum);
		result.fadeIn(0);
		
		if (correctNum >= 15)
		{
			result.addClass("result-correct");
			timeout[2] = setTimeout(function(){
				audio[0].play();
				result.fadeOut(0);
				award.fadeIn(1000);
				snowfall.fadeIn(1000);
				snowfallSprite.play();
				saltContainer.append("<div class='salt'></div>");
			}, 3000);
		}
		else
			result.addClass("result-false");
		
		timeout[3] = setTimeout(function(){
			for(var i = 0; i < answerfields.length; i++)
			{
				var min = parseInt($(answerfields[i]).attr("data-key-min"));
				var max = parseInt($(answerfields[i]).attr("data-key-max"));
				var answer = parseInt($(answerfields[i]).val());
				
				if ((answer < min) || (answer > max)) {
					$(answerfields[i]).val(min)
					$(answerfields[i]).css("color", "green");
				}
			}
		}, 3000);
		
		timeout[4] = setTimeout(function(){
			result.fadeOut(0);
			award.fadeOut(0);
			snowfallSprite.pause();
			snowfall.fadeOut(0);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(1);
		}, 7000);
	}
	checkButton.off("click", checkListener);
	checkButton.on("click", checkListener);
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(7);
	}, 2000);
}

launch["frame-108"]  = function()
{
		theFrame = $("#frame-108"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		answers = $(prefix + ".answer"),
		checkboxes = $(prefix + ".check"),
		checkButton = $(prefix + ".check-button"),
		checkAnswer = $(prefix + ".check-answer .word"),
		checkAnswer2 = $(prefix + ".check-answer .check"),
		result = $(prefix + ".result"),
		award = $(prefix + ".award"),
		snowfall = $(prefix + ".snowfall"),
		correctNum = 0,
		saltContainer = $(".salt-container"),
		exercises = $(".task-exercise"), 
		checkedNum = 0;
	
	var snowfallSprite = new Motio(snowfall[0], {
		fps: 3,
		frames: 3
	});
	
	audioPieces[0] = new AudioPiece("s8-1", 0, 9);
	audioPieces[1] = new AudioPiece("s8-1", 11, 99);
	audio[0] = new Audio("audio/award-sound.mp3");
	
	fadeNavsOut();
	fadeLauncherIn();
	checkButton.fadeOut(0);
	result.fadeOut(0);
	award.fadeOut(0);
	snowfall.fadeOut(0);
	
	checkboxes.prop("checked", false);
		
	startButtonListener = function(){
		audioPieces[0].play();
	};
		
	var answerListener = function(){
		$(this).css("font-weight", "bold");
		$(this).siblings().css("font-weight", "");
	}
	answers.off("click", answerListener);
	answers.on("click", answerListener);
	
	var checkListener = function()
	{
		for(var i = 0; i < answers.length; i++)
		{
			if ($(answers[i]).css("font-weight") === "700")
			{
				$(answers[i]).css("text-shadow", "0px 0px 5px white, 0px 0px 5px white, 0px 0px 5px white");
				$(answers[i]).css("color", "red");
			}
			
			if ($(answers[i]).attr("data-correct"))
			{
				correctNum ++;
				$(answers[i]).css("text-shadow", "0px 0px 5px white, 0px 0px 5px white, 0px 0px 5px white");
				$(answers[i]).css("color", "green");
				$(answers[i]).css("font-weight", "bold");
			}
		}			
		
		for(var i = 0; i < checkboxes.length; i++)
		{
			if ($(checkboxes[i]).prop("checked").toString() === $(checkboxes[i]).attr("data-correct"))
			{
				correctNum ++;
				$(checkboxes[i]).parent().css("text-shadow", "0px 0px 5px white, 0px 0px 5px white, 0px 0px 5px white");
				$(checkboxes[i]).parent().css("color", "green");
				$(checkboxes[i]).parent().css("font-weight", "bold");
			}
			else if($(checkboxes[i]).prop("checked"))
			{
				$(checkboxes[i]).parent().css("text-shadow", "0px 0px 5px white, 0px 0px 5px white, 0px 0px 5px white");
				$(checkboxes[i]).parent().css("color", "red");
				$(checkboxes[i]).parent().css("font-weight", "bold");
			}
			else if($(checkboxes[i]).attr("data-correct"))
			{
				$(checkboxes[i]).parent().css("text-shadow", "0px 0px 5px white, 0px 0px 5px white, 0px 0px 5px white");
				$(checkboxes[i]).parent().css("color", "green");
				$(checkboxes[i]).parent().css("font-weight", "bold");
			}
		}
		result.append("<br>" + correctNum);
		
		result.fadeIn(0);
		
		if (correctNum === 7)
		{
			result.addClass("result-correct");
			timeout[0] = setTimeout(function(){
				result.fadeOut(0);
				award.fadeIn(0);
				snowfall.fadeIn(0);
				saltContainer.append("<div class='salt'></div>");
				audio[0].play();
				snowfallSprite.play();
			}, 3000);
		}
		else
			result.addClass("result-false");
		
		timeout[1] = setTimeout(function(){
			audioPieces[1].play();
		}, 5000);
		
		timeout[2] = setTimeout(function(){
			result.fadeOut(0);
			award.fadeOut(0);
			snowfall.fadeOut(0);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(8);
		}, 10000);
	}
	checkButton.off("click", checkListener);
	checkButton.on("click", checkListener);
	
	var checkAnswerListener = function(){
		currElem = $(this);
		
		if(currElem.siblings().prop("checked") === false)
		{
			if(checkedNum < 6)
			{
				currElem.siblings().prop("checked", true);
				checkedNum++;
			}
		}
		else
		{
			currElem.siblings().prop("checked", false);
			checkedNum--;
		}
		
		checkButton.fadeIn(0);
	}
	checkAnswer.off("click", checkAnswerListener);
	checkAnswer.on("click", checkAnswerListener);
	
	var checkAnswerListener2 = function(){
		currElem = $(this);
		
		if(currElem.prop("checked") === false)
		{
			checkedNum--;
		}
		else
		{
			checkedNum++;
		}
		
		if(checkedNum > 6)
		{
			currElem.prop("checked", false);
			checkedNum--;
		}
		
		checkButton.fadeIn(0);
	}
	checkAnswer2.off("click", checkAnswerListener2);
	checkAnswer2.on("click", checkAnswerListener2);
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(8);
	}, 2000);
}

launch["frame-109"]  = function()
{
		theFrame = $("#frame-109"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		textContainer = $(prefix + ".text-container"), 
		yunchen = $(prefix + ".s9-yuchen, " + prefix + ".yuchen-label"),
		tuzdybas = $(prefix + ".s9-tuzdybas, " + prefix + ".tuzdybas-label");
	
	typingText = new TypingText(textContainer, 20, true, function(){
		tuzdybas.fadeIn(500);
		timeout[0] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(9);
		}, 3000);
	});
		
	fadeNavsOut();
	fadeLauncherIn();
	yunchen.fadeOut(0);
	tuzdybas.fadeOut(0);
	textContainer.fadeOut(0);
	
	startButtonListener = function(){
		textContainer.html("");
		textContainer.fadeIn(300);
		
		timeout[0] = setTimeout(function(){
			typingText.write();
		}, 2000);
		
		timeout[1] = setTimeout(function(){
			yunchen.fadeIn(500);
		}, 5000);
	};
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(9);
	}, 2000);
}

launch["frame-201"] = function(){
	theFrame = $("#frame-201"),
	theClone = theFrame.clone();
};

launch["frame-202"] = function(){
	theFrame = $("#frame-202"),
	theClone = theFrame.clone();
};

launch["frame-203"] = function(){
	theFrame = $("#frame-203"),
	theClone = theFrame.clone();
};

launch["frame-204"] = function(){
	theFrame = $("#frame-204"),
	theClone = theFrame.clone();
};

launch["frame-301"]  = function()
{
		theFrame = $("#frame-301"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		nextButton = $(prefix + ".next-task-button"),
		prevButton = $(prefix + ".prev-task-button"),
		checkButton = $(prefix + ".check-button"),
		result = $(prefix + ".result"),
		award = $(prefix + ".award"),
		snowfall = $(prefix + ".snowfall"),
		answers = $(prefix + ".answer"),
		questionsAnswers = $(prefix + ".task, " + prefix + ".answer"),
		questions1 = $(prefix + ".task-1, " + prefix + ".task-2, " + prefix + ".task-3"),
		questions2 = $(prefix + ".task-4, " + prefix + ".task-5"),
		correctNum = 0;
	
	var snowfallSprite = new Motio(snowfall[0], {
		fps: 3,
		frames: 3
	});
	
	audio[0] = new Audio("audio/award-sound.mp3");
	
	nextButton.fadeOut(0);
	prevButton.fadeOut(0);
	checkButton.fadeOut(0);
	questions2.fadeOut(0);
	questions1.fadeOut(0);
	result.fadeOut(0);
	award.fadeOut(0);
	snowfall.fadeOut(0);
	fadeNavsOut();
	fadeLauncherIn();
	
	checkButtonListener = function()
	{
		for (var i = 0; i < answers.length; i++)
		{
			if ($(answers[i]).hasClass("bold") && $(answers[i]).attr("data-correct"))
			{
				correctNum ++;
			}
			
			if($(answers[i]).hasClass("bold") && !$(answers[i]).attr("data-correct"))
			{
				$(answers[i]).css("text-shadow", "0px 0px 5px white, 0px 0px 5px white, 0px 0px 5px white");
				$(answers[i]).css("color", "red");
			}
			
			if($(answers[i]).attr("data-correct"))
			{
				$(answers[i]).addClass("bold");
				$(answers[i]).css("text-shadow", "0px 0px 5px white, 0px 0px 5px white, 0px 0px 5px white");
				$(answers[i]).css("color", "green");
			}
		}
		
		result.append(correctNum);
		result.fadeIn(0);
		if (correctNum >= 5)
		{
			result.addClass("result-correct");
			timeout[0] = setTimeout(function(){
				result.fadeOut(0);
				audio[0].play();
				saltContainer.append("<div class='salt'></div>");
				award.fadeIn(0);
				snowfall.fadeIn(0);
				snowfallSprite.play();
			}, 3000);
		}
		else
			result.addClass("result-false");
				
		questions2.fadeOut(500);
		questions1.fadeIn(500);
		
		timeout[1] = setTimeout(function(){
			questions2.fadeIn(500);
			questions1.fadeOut(500);
		}, 5000);
		
		timeout[2] = setTimeout(function(){
			award.fadeOut(0);
			snowfall.fadeOut(0);
			snowfallSprite.pause();
			result.fadeOut(0);
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(31);
		}, 10000);	
	}
	checkButton.off("click", checkButtonListener);
	checkButton.on("click", checkButtonListener);
	
	var nextButtonListener = function()
	{
		questions1.fadeOut(0);
		questions2.fadeIn(0);
		nextButton.fadeOut(0);
		prevButton.fadeIn(0);
		checkButton.fadeIn(0);
	}
	nextButton.off("click", nextButtonListener);
	nextButton.on("click", nextButtonListener);
	
	var prevButtonListener = function()
	{
		questions2.fadeOut(0);
		questions1.fadeIn(0);
		nextButton.fadeIn(0);
		prevButton.fadeOut(0);
		checkButton.fadeOut(0);
	}
	prevButton.off("click", prevButtonListener);
	prevButton.on("click", prevButtonListener);
	
	startButtonListener = function(){
		questions1.fadeIn(1000);
	};
		
	var answerListener = function(){
		var currAnswer = $(this);
		
		currAnswer.siblings().removeClass("bold");
		currAnswer.addClass("bold");
		
		if($(".bold").length === 3)
			nextButton.fadeIn(0);
	};
	answers.off("click", answerListener);
	answers.on("click", answerListener);
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(31);
	}, 2000);
}

launch["frame-401"]  = function()
{
		theFrame = $("#frame-401"),
		theClone = theFrame.clone();
	var	prefix = "#" + theFrame.attr("id") + " ",
		clams = $(prefix + ".clams"),
		glass2 = $(prefix + ".glass-2"),
		stand = $(prefix + ".stand"),
		spatula = $(prefix + ".spatula"), 
		flameAnimated = $(prefix + ".flame-animated"),
		light = $(prefix + ".light"),
		match = $(prefix + ".match"),
		burner = $(prefix + ".burner"),
		steam = $(prefix + ".steam"),
		scales30 = $(prefix + ".scales-30"),
		scales60 = $(prefix + ".scales-60"),
		resultLabel = $(prefix + ".result-label");
	
	var clamsSprite = new Motio(clams[0], {
		"fps": "3",
		"frames": "3"
	});
	
	var glass2Sprite = new Motio(glass2[0], {
		"fps": "3",
		"frames": "3"
	});
	glass2Sprite.to(2, true);
	
	var standSprite = new Motio(stand[0], {
		"fps": "0.5",
		"frames": "6"
	});
	standSprite.to(1, true);
	standSprite.on("frame", function(){
		if (this.frame === this.frames - 1) {
			this.pause();
			flameAnimated.fadeOut(0);
			steamSprite.pause();
			steam.fadeOut(0);
		}
	});
	
	var steamSprite = new Motio(steam[0], {
		"fps": "3",
		"frames": "2"
	});
	
	audio[0] = new Audio("audio/water-pour.mp3");
	audio[1] = new Audio("audio/match-sound.mp3");
	audio[2] = new Audio("audio/fire-burn.mp3");
	audio[3] = new Audio("audio/glass-sound.mp3");
	audio[4] = new Audio("audio/salt-sound.mp3");
	
	fadeNavsOut();
	fadeLauncherIn();
	flameAnimated.fadeOut(0);
	light.fadeOut(0);
	steam.fadeOut(0);
	scales60.fadeOut(0);
	scales30.fadeOut(0);
	resultLabel.fadeOut(0);
		
	startButtonListener = function(){
		blink(glass2, 500, 3);
		timeout[0] = setTimeout(function(){
			blink(stand, 500, 3);	
		},2000);
		timeout[1] = setTimeout(function(){
			audio[0].play();
			standSprite.to(2, true);
			glass2Sprite.to(0, true);
			scales30.fadeIn(0);
		},4000);
		timeout[2] = setTimeout(function(){
			blink(match, 500, 2);
		},6000);
		timeout[3] = setTimeout(function(){
			blink(burner, 500, 2);
		},7000);
		timeout[4] = setTimeout(function(){
			audio[1].play();
			match.fadeOut(0);
			flameAnimated.fadeIn(0);
		},9000);
		timeout[5] = setTimeout(function(){
			steam.fadeIn(0);
			steamSprite.play();
			audio[2].play();
			standSprite.play();
		},10000);
		timeout[6] = setTimeout(function(){
			blink(clams, 500, 2);
		},13000);
		timeout[7] = setTimeout(function(){
			blink(stand, 500, 2);
		},14000);
		timeout[8] = setTimeout(function(){
			audio[3].play();
			clamsSprite.to(1, true);
			standSprite.to(0, true);
		},16000);
		timeout[9] = setTimeout(function(){
			blink(spatula, 500, 2);
		},19000);
		timeout[10] = setTimeout(function(){
			blink(clams, 500, 2);
		},20000);
		timeout[11] = setTimeout(function(){
			blink(glass2, 500, 2);
		},21000);
		timeout[12] = setTimeout(function(){
			audio[4].play();
			clamsSprite.to(2, true);
			glass2Sprite.to(1, true);
			scales30.fadeOut(0);
			scales60.fadeIn(0);
		},22000);
		timeout[13] = setTimeout(function(){
			resultLabel.fadeIn(0);
		},24000);
		timeout[13] = setTimeout(function(){
			resultLabel.addClass("box-shadow-white");
		},25000);
		timeout[13] = setTimeout(function(){
			theFrame.attr("data-done", "true");
			fadeNavsInAuto();
			sendCompletedStatement(41);
		},32000);
	};
	
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(41);
	}, 2000);
}

launch["frame-501"]  = function()
{
	theFrame = $("#frame-501"),
	theClone = theFrame.clone();
	
	fadeNavsOut();
	fadeLauncherIn();
	
	startButtonListener = function(){
		
	}
	timeout[0] = setTimeout(function(){
		startButtonListener();
		sendLaunchedStatement(51);
	}, 2000);
}

var hideEverythingBut = function(elem)
{
	if(theFrame)
		theFrame.html(theClone.html());
	
	var frames = $(".frame"), 
		elemId = elem.attr("id");
	
	frames.fadeOut(0);
	elem.fadeIn(0);
	regBox = $(".reg-box");
	fadeTimerOut();
	
	if(!globalName)
		regBox.fadeOut(0);
	
	if(elemId === "frame-000")
	{
		fadeNavsOut();
		fadeTimerOut();
	}
	
	for (var i = 0; i < audio.length; i++)
		audio[i].pause();	
	
	for (var i = 0; i < audioPieces.length; i++)
		audioPieces[i].pause();	

	for (var i = 0; i < timeout.length; i++)
		clearTimeout(timeout[i]);
	
	if(elem.attr("id") === "frame-000")
		regBox.fadeIn(0);
	
	if(launch[elem.attr("id")])
		launch[elem.attr("id")]();
	
	initMenuButtons();
	
	if(typingText)
		typingText.stopWriting();
	
	saltContainer = $(".salt-container");
		
	if(elem.attr("id") === "frame-000")
		fadeNavsOut();		
	
	if(elem.attr("id") === "frame-105" ||
	elem.attr("id") === "frame-106" ||
	elem.attr("id") === "frame-107" ||
	elem.attr("id") === "frame-108" ||
	elem.attr("id") === "frame-109" ||
	elem.attr("id") === "frame-301")
		saltContainer.fadeIn(0);
	else
		saltContainer.fadeOut(0);	
}

var initMenuButtons = function(){
	var links = $(".link");
	links.click(function(){
		var elem = $("#"+$(this).attr("data-link"));
		hideEverythingBut(elem);
	});
};

var main = function()
{
	var video = $(".intro-video"),
		pic = $(".intro-pic");
		
	initMenuButtons();
	hideEverythingBut($("#frame-000"));
	
	setMenuStuff();
	video.attr("width", video.parent().css("width"));
	video.attr("height", video.parent().css("height"));
	video[0].play(); 
	
	timeout[0] = setTimeout(function(){
		video.hide();
	}, 10000);
	timeout[1] = setTimeout(function(){
		pic.hide(); 
    }, 15000);
};

$(document).ready(main);