(function(w, callback) {
  /**
   * @param {HTMLElement} element
   * @param {Object} options
   * @return {undefined}
   */
  function Motio(element, options) {
    /**
     * @param {boolean} reversed
     * @return {undefined}
     */
    function resume(reversed) {
      /** @type {boolean} */
      animation.reversed = reversed;
      if (!frameID) {
        /** @type {boolean} */
        self.isPaused = false;
        trigger("play");
        frameID = rAF(render);
      }
    }
    /**
     * @return {undefined}
     */
    function render() {
      frameID = rAF(render);
      var input = readFile();
      if (!(60 > o.fps && (animation.lastFrame && animation.lastFrame + 1E3 / o.fps + 1 > input))) {
        animation.lastFrame = input;
        if (isPan) {
          pos.x += o.speedX / o.fps;
          pos.y += o.speedY / o.fps;
          if (o.bgWidth) {
            if (Math.abs(pos.x) > o.bgWidth) {
              pos.x %= o.bgWidth;
            }
          }
          if (o.bgHeight) {
            if (Math.abs(pos.y) > o.bgHeight) {
              pos.y %= o.bgHeight;
            }
          }
        } else {
          if (animation.finite) {
            active = animation.immediate ? animation.to : active + (active > animation.to ? -1 : 1);
          } else {
            if (animation.reversed) {
              if (0 >= --active) {
                /** @type {number} */
                active = frames.length - 1;
              }
            } else {
              if (++active >= frames.length) {
                /** @type {number} */
                active = 0;
              }
            }
          }
          self.frame = active;
        }
        bgPos = isPan ? Math.round(pos.x) + "px " + Math.round(pos.y) + "px" : frames[active];
        if (bgPos !== lastPos) {
          element.style.backgroundPosition = lastPos = bgPos;
        }
        trigger("frame");
        if (animation.finite) {
          if (animation.to === active) {
            self.pause();
            if ("function" === type(animation.callback)) {
              animation.callback.call(self);
            }
          }
        }
      }
    }
    /**
     * @param {?} name
     * @param {Object} fn
     * @return {?}
     */
    function callbackIndex(name, fn) {
      /** @type {number} */
      i = 0;
      valsLength = result[name].length;
      for (;i < valsLength;i++) {
        if (result[name][i] === fn) {
          return i;
        }
      }
      return-1;
    }
    /**
     * @param {string} name
     * @param {?} data
     * @return {undefined}
     */
    function trigger(name, data) {
      if (result[name]) {
        /** @type {number} */
        i = 0;
        valsLength = result[name].length;
        for (;i < valsLength;i++) {
          result[name][i].call(self, name, data);
        }
      }
    }
    /**
     * @param {string} name
     * @return {?}
     */
    function getProp(name) {
      return w.getComputedStyle ? w.getComputedStyle(element, null)[name] : element.currentStyle[name];
    }
    var o = defaults(options);
    var self = this;
    /** @type {boolean} */
    var isPan = !o.frames;
    /** @type {Array} */
    var frames = [];
    var result = {};
    var animation = {};
    /** @type {number} */
    var active = 0;
    var pos;
    var bgPos;
    var lastPos;
    var frameID;
    var i;
    var valsLength;
    /** @type {HTMLElement} */
    self.element = element;
    self.width = o.width || element.clientWidth;
    self.height = o.height || element.clientHeight;
    self.options = o;
    /** @type {boolean} */
    self.isPaused = true;
    /**
     * @return {?}
     */
    self.pause = function() {
      cAF(frameID);
      /** @type {number} */
      frameID = 0;
      /** @type {number} */
      animation.lastFrame = 0;
      if (!self.isPaused) {
        /** @type {boolean} */
        self.isPaused = true;
        trigger("pause");
      }
      return self;
    };
    /**
     * @param {boolean} reversed
     * @return {?}
     */
    self.play = function(reversed) {
      /** @type {boolean} */
      animation.finite = false;
      /** @type {Function} */
      animation.callback = callback;
      /** @type {boolean} */
      animation.immediate = false;
      resume(reversed);
      return self;
    };
    /**
     * @return {?}
     */
    self.toggle = function() {
      self[frameID ? "pause" : "play"]();
      return self;
    };
    /**
     * @param {Object} immediate
     * @param {Function} callback
     * @return {?}
     */
    self.toStart = function(immediate, callback) {
      return self.to(0, immediate, callback);
    };
    /**
     * @param {Object} immediate
     * @param {Function} callback
     * @return {?}
     */
    self.toEnd = function(immediate, callback) {
      return self.to(frames.length - 1, immediate, callback);
    };
    /**
     * @param {number} item
     * @param {Object} immediate
     * @param {Function} callback
     * @return {?}
     */
    self.to = function(item, immediate, callback) {
      if (isPan || (isNaN(parseFloat(item)) || (!isFinite(item) || (0 > item || item >= frames.length)))) {
        return self;
      }
      if ("function" === type(immediate)) {
        /** @type {Object} */
        callback = immediate;
        /** @type {boolean} */
        immediate = false;
      }
      if (item === active) {
        if (0 === item) {
          /** @type {number} */
          active = frames.length;
        } else {
          if (item === frames.length - 1) {
            /** @type {number} */
            active = -1;
          } else {
            return "function" === type(callback) && callback.call(self), self.pause(), self;
          }
        }
      }
      /** @type {boolean} */
      animation.finite = true;
      /** @type {number} */
      animation.to = item;
      /** @type {boolean} */
      animation.immediate = !!immediate;
      /** @type {Function} */
      animation.callback = callback;
      resume();
      return self;
    };
    /**
     * @param {?} key
     * @param {?} value
     * @return {?}
     */
    self.set = function(key, value) {
      o[key] = value;
      return self;
    };
    /**
     * @param {Object} name
     * @param {(Object|string)} fn
     * @return {?}
     */
    self.on = function(name, fn) {
      if ("object" === type(name)) {
        var key;
        for (key in name) {
          if (name.hasOwnProperty(key)) {
            self.on(key, name[key]);
          }
        }
      } else {
        if ("function" === type(fn)) {
          key = name.split(" ");
          /** @type {number} */
          var l = 0;
          var ll = key.length;
          for (;l < ll;l++) {
            result[key[l]] = result[key[l]] || [];
            if (-1 === callbackIndex(key[l], fn)) {
              result[key[l]].push(fn);
            }
          }
        } else {
          if ("array" === type(fn)) {
            /** @type {number} */
            key = 0;
            l = fn.length;
            for (;key < l;key++) {
              self.on(name, fn[key]);
            }
          }
        }
      }
      return self;
    };
    /**
     * @param {string} types
     * @param {Object} fn
     * @return {?}
     */
    self.off = function(types, fn) {
      if (fn instanceof Array) {
        /** @type {number} */
        var p = 0;
        /** @type {number} */
        var l = fn.length;
        for (;p < l;p++) {
          self.off(types, fn[p]);
        }
      } else {
        p = types.split(" ");
        /** @type {number} */
        l = 0;
        var pl = p.length;
        for (;l < pl;l++) {
          if (result[p[l]] = result[p[l]] || [], "undefined" === type(fn)) {
            /** @type {number} */
            result[p[l]].length = 0;
          } else {
            var index = callbackIndex(p[l], fn);
            if (-1 !== index) {
              result[p[l]].splice(index, 1);
            }
          }
        }
      }
      return self;
    };
    /**
     * @return {?}
     */
    self.destroy = function() {
      self.pause();
      /** @type {string} */
      element.style.backgroundPosition = "";
      return self;
    };
    (function() {
      var i = (getProp("backgroundPosition") || getProp("backgroundPositionX") + " " + getProp("backgroundPositionY")).replace(/left|top/gi, 0).split(" ");
      pos = {
        x : 0 | parseInt(i[0], 10),
        y : 0 | parseInt(i[1], 10)
      };
      if (isPan) {
        self.pos = pos;
      } else {
        /** @type {number} */
        i = frames.length = 0;
        for (;i < o.frames;i++) {
          if (o.vertical) {
            /** @type {number} */
            pos.y = i * -self.height;
          } else {
            /** @type {number} */
            pos.x = i * -self.width;
          }
          frames.push(pos.x + "px " + pos.y + "px");
        }
        /** @type {number} */
        self.frames = frames.length;
        /** @type {number} */
        self.frame = 0;
      }
    })();
  }
  /**
   * @param {Object} value
   * @return {?}
   */
  function type(value) {
    return null == value ? String(value) : "object" === typeof value || "function" === typeof value ? value instanceof w.NodeList && "nodelist" || (value instanceof w.HTMLCollection && "htmlcollection" || Object.prototype.toString.call(value).match(/\s([a-z]+)/i)[1].toLowerCase()) : typeof value;
  }
  /**
   * @param {Object} options
   * @return {?}
   */
  function defaults(options) {
    var ret = {};
    options = "object" === type(options) ? options : {};
    var key;
    for (key in Motio.defaults) {
      ret[key] = (options.hasOwnProperty(key) ? options : Motio.defaults)[key];
    }
    return ret;
  }
  /** @type {function (this:Window, number): ?} */
  var cAF = w.cancelAnimationFrame || w.cancelRequestAnimationFrame;
  /** @type {function (this:Window, function (number): ?, (Element|null)=): number} */
  var rAF = w.requestAnimationFrame;
  (function() {
    /** @type {Array} */
    var vendors = ["moz", "webkit", "o"];
    /** @type {number} */
    var lastTime = 0;
    /** @type {number} */
    var i = 0;
    /** @type {number} */
    var l = vendors.length;
    for (;i < l && !cAF;++i) {
      rAF = (cAF = w[vendors[i] + "CancelAnimationFrame"] || w[vendors[i] + "CancelRequestAnimationFrame"]) && w[vendors[i] + "RequestAnimationFrame"];
    }
    if (!cAF) {
      /**
       * @param {Function} callback
       * @return {?}
       */
      rAF = function(callback) {
        /** @type {number} */
        var currTime = +new Date;
        /** @type {number} */
        var timeToCall = Math.max(0, 16 - (currTime - lastTime));
        /** @type {number} */
        lastTime = currTime + timeToCall;
        return w.setTimeout(function() {
          callback(currTime + timeToCall);
        }, timeToCall);
      };
      /**
       * @param {?} id
       * @return {undefined}
       */
      cAF = function(id) {
        clearTimeout(id);
      };
    }
  })();
  var readFile = function() {
    /** @type {(Performance|null)} */
    var d = w.performance;
    return d && d.now ? d.now.bind(d) : function() {
      return+new Date;
    };
  }();
  /** @type {function (HTMLElement, Object): undefined} */
  w.Motio = Motio;
  Motio.defaults = {
    fps : 15,
    frames : 0,
    vertical : 0,
    width : 0,
    height : 0,
    speedX : 0,
    speedY : 0,
    bgWidth : 0,
    bgHeight : 0
  };
})(window);
